//
//  ApiViewController.swift
//  Example
//
//  Created by lee@sidechef.com on 04/11/2024.
//  Copyright (c) 2024 lee@sidechef.com. All rights reserved.
//


import UIKit
import SideChefEmbeddedSDK
import WebKit

class ApiViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var tableView = UITableView()
    var nextPageUrlString: String?
    var data: [SCRecipe] = []
    let cellReuseIdentifier = "cell"

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        }
        tableView.register(RecipeCardCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        view.addSubview(tableView)
        tableView.bindFrameToSuperviewBounds()
        tableView.delegate = self
        tableView.dataSource = self
        
        loadMore()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? RecipeCardCell else {
            return UITableViewCell()
        }
        let recipe = data[indexPath.row]
        cell.configure(with: recipe)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIButton(type: .system)
        
        if nextPageUrlString == nil {
            footer.setTitle("No more data", for: .normal)
        } else {
            footer.setTitle("Load more...", for: .normal)
            if #available(iOS 13.0, *) {
                footer.backgroundColor = .secondarySystemBackground
            }
            footer.addTarget(self, action: #selector(loadMore), for: .touchUpInside)
        }

        footer.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 50)
        return footer
    }
    
    @objc
    func loadMore() {
        SCAPI.shared.homePopular.getNextPage { result in
            switch result {
            case .success(let listResult):
                DispatchQueue.main.async { [self] in
                    data.append(contentsOf: listResult.results)
                    
                    nextPageUrlString = listResult.next
                    tableView.reloadData()
                }
            case .failure(let error):
                if let decodingError = error as? DecodingError {
                    print("JSON Decoding Error: \(decodingError)")
                } else {
                    print("API call failed: ", error)
                }
            }
        }
    }
}

class RecipeCardCell: UITableViewCell {
    private let contentPadding: CGFloat = 8.0
    private let imageSideLength: CGFloat = 120.0
    
    let nameLabel = UILabel()
    let customImageView = UIImageView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    private func setupViews() {
        customImageView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(customImageView)
        contentView.addSubview(nameLabel)

        NSLayoutConstraint.activate([
            customImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: contentPadding),
            customImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            customImageView.widthAnchor.constraint(equalToConstant: imageSideLength),
            customImageView.heightAnchor.constraint(equalToConstant: imageSideLength),

            nameLabel.leadingAnchor.constraint(equalTo: customImageView.trailingAnchor, constant: contentPadding),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -contentPadding),
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: contentPadding),
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -contentPadding)
        ])
        
        customImageView.contentMode = .scaleAspectFill
        customImageView.clipsToBounds = true
    }
    
    func configure(with recipe: SCRecipe) {
        nameLabel.text = recipe.name
        setImage(from: recipe.coverPicURLSmall)
    }
    
    private var currentURLString: String?
    private var currentDownloadTask: URLSessionDataTask?

    private func setImage(from urlString: String?) {
        currentDownloadTask?.cancel()

        guard let urlString = urlString, let url = URL(string: urlString) else {
            self.customImageView.image = nil
            return
        }

        self.currentURLString = urlString
        self.currentDownloadTask = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let self = self, let data = data, error == nil else { return }

            DispatchQueue.main.async {
                if self.currentURLString == urlString {
                    self.customImageView.image = UIImage(data: data)
                }
            }
        }
        self.currentDownloadTask?.resume()
    }
}
