//
//  MenuViewController.swift
//  Example
//
//  Created by lee@sidechef.com on 04/11/2024.
//  Copyright (c) 2024 lee@sidechef.com. All rights reserved.
//


import UIKit

class MenuViewController: UIViewController {
    var btnGoWebViewScreen = UIButton(type: .system)
    var btnGoApiScreen = UIButton(type: .system)

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        btnGoWebViewScreen.setTitle("WebView Screen", for: .normal)
        btnGoWebViewScreen.backgroundColor = .black
        btnGoWebViewScreen.addTarget(self, action: #selector(gotoWebViewScreen), for: .touchUpInside)
        view.addSubview(btnGoWebViewScreen)

        btnGoWebViewScreen.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            btnGoWebViewScreen.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),
            btnGoWebViewScreen.heightAnchor.constraint(equalToConstant: 40),
            btnGoWebViewScreen.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            btnGoWebViewScreen.topAnchor.constraint(equalTo: view.topAnchor, constant: 100)
        ])

        btnGoApiScreen.setTitle("Api Screen", for: .normal)
        btnGoApiScreen.backgroundColor = .black
        btnGoApiScreen.addTarget(self, action: #selector(gotoApiScreen), for: .touchUpInside)
        view.addSubview(btnGoApiScreen)

        btnGoApiScreen.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            btnGoApiScreen.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),
            btnGoApiScreen.heightAnchor.constraint(equalToConstant: 40),
            btnGoApiScreen.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            btnGoApiScreen.topAnchor.constraint(equalTo: view.topAnchor, constant: 160)
        ])
    }

    @objc
    func gotoWebViewScreen() {
        self.navigationController?.pushViewController(WebViewController(), animated: true)
    }
    @objc
    func gotoApiScreen() {
        self.navigationController?.pushViewController(ApiViewController(), animated: true)
    }
}
