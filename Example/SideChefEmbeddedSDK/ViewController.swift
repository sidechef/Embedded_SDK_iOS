//
//  ViewController.swift
//  SideChefEmbeddedSDK
//
//  Created by lee@sidechef.com on 04/11/2024.
//  Copyright (c) 2024 lee@sidechef.com. All rights reserved.
//

import UIKit

class ViewController: UINavigationController {
    var menu = MenuViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.pushViewController(menu, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

