//
//  SCWebViewController.swift
//  Example
//
//  Created by lee@sidechef.com on 04/11/2024.
//  Copyright (c) 2024 lee@sidechef.com. All rights reserved.
//

import UIKit
import SideChefEmbeddedSDK
import WebKit

class WebViewController: UIViewController {
    var webView = SCWebView(initialPage: .none)!

    override func viewDidLoad() {
        super.viewDidLoad()

        var backButton: UIBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBackButtonPressed))
        if #available(iOS 13.0, *) {
           let backImage = UIImage(systemName: "arrow.left")
           backButton = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(handleBackButtonPressed))
        }
        navigationItem.leftBarButtonItem = backButton
        view.addSubview(webView)
        webView.hasProgress = true
        webView.bindFrameToSuperviewBounds()
    }
}

// Use webView.goBackIfNeeded() func to control goBack.
extension WebViewController {
    @objc func handleBackButtonPressed() {
        // Try to go back in the web view first
        if !webView.goBackIfNeeded() {
            // If it cannot go back in the web view, perform native navigation action
            navigationController?.popViewController(animated: true)
        }
    }
}

