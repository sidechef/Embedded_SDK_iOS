# SideChefEmbeddedSDK

## Requirements

* iOS 12.0+
* Swift 5.10+

## Installation

SideChefEmbeddedSDK is available through [CocoaPods](https://cocoapods.org). 

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate SideChefEmbeddedSDK into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
platform :ios, '12.0'
source 'https://gitlab.com/sidechef/sidechef-repo.git'

use_frameworks!

target '<Target Name>' do
	pod 'SideChefEmbeddedSDK'
end
```

Then, run the following command:

```bash
$ pod install
```



## Usage

### Initializing the SDK

Import SideChefCXP module

```swift
import SideChefEmbeddedSDK

```

Configure SDK, typically in your App's initializer or app delegate's application(_:didFinishLaunchingWithOptions:) method:

```swift
SideChefEmbeddedSDK.configure(
    apiKey: "api key",
    userInfo: SCUserInfo(id: "42", name: "John Doe", photoURL: "https://www.example.com/photos/42.png", email: "john.doe@example.com")
)
```

### Usage of SCWebView
Create SCWebView, and add to your view:

```swift
var webView = SCWebView()

view.addSubview(webView)
```

### Usage of SCAPI

```swift
SCAPI.shared.homePopular.getNextPage { result in
    switch result {
    case .success(let listResult):
        print("home popular is", listResult.results)
    case .failure(let error):
        print("fetch home popular failed -- ", error)
    }
}
```
## API

### `SideChefView` props
| Property | Description | Type | Required | Default |
| :--- | :--- | :---: | :---: | :---: |
| `hasProgress` | Display a progress indicator bar on the top of the view | boolean | no | `true` |
| `progressView` | Progress bar | UIProgressView | no | system default |



## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Author

Roland Lee: lee@sidechef.com

## License

SideChefEmbeddedSDK is available under the MIT license. See the LICENSE file for more info.
