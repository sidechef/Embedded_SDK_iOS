#
# Be sure to run `pod lib lint SideChefEmbeddedSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SideChefEmbeddedSDK'
  s.version          = '0.1.5'
  s.summary          = 'SideChefEmbeddedSDK.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/sidechef/Embedded_SDK_iOS'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'SideChef' => 'lee@sidechef.com' }
  s.source           = { :git => 'https://gitlab.com/sidechef/Embedded_SDK_iOS.git', :tag => s.version.to_s }

  s.ios.deployment_target = '12.0'

  s.source_files = 'SideChefEmbeddedSDK/Classes/**/*'

  # s.resource_bundles = {
  #   'SideChefEmbeddedSDK' => ['SideChefEmbeddedSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
