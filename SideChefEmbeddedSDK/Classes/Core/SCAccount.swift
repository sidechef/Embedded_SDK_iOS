//
//  SCAccount.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/9/29.
//

import Foundation
import Security
import UIKit

let service = "com.sidechef.com"

public class SCAccount: NSObject {
    static let shared: SCAccount = SCAccount()
    var uuid: String {
        guard let _uuid = get(propertyName: "uuid") else {
            return save(propertyName: "uuid", value: UUID().description)
        }
        return _uuid
    }
    
    var userID: Int? {
        get {
            guard let uid = get(propertyName: "userID") else {
                return nil
            }
            return Int(uid) ?? nil
        }
        set {
            guard let uid = newValue else {
                return
            }
            _ = save(propertyName: "userID", value: "\(uid)")
        }
    }
    
    public static func getUserAgent() -> String {
        let name = "SideChef Embedded SDK"
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "UnknownVersion"
        let deviceType = UIScreen.main.traitCollection.userInterfaceIdiom == .phone ? "iPhone" : "iPad"
        let deviceOS = "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)"
        let scaleFactor = UIScreen.main.scale
        let partner = SCAuthenticator.shared.partnerKey ?? ""
        let uuid = SCAccount.shared.uuid
        let uid = SCAccount.shared.userID == nil ? "" : "/\(SCAccount.shared.userID!)"
        return "\(name)/\(version) (\(deviceType); \(deviceOS); Scale/\(scaleFactor)) \(partner)/\(uuid)\(uid)"
    }
    
    func get(propertyName: String) -> String? {
        var query = getQuery(propertyName: propertyName)
        query[kSecReturnData as String] = true
        query[kSecReturnAttributes as String] = true
        query[kSecMatchLimit as String] = kSecMatchLimitOne
        var itemCopy: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &itemCopy)
        guard status == errSecSuccess else {
            return nil
        }
        
        guard let value = itemCopy as? [String: AnyObject],
              let data = value[kSecValueData as String] as? Data,
              let str = String(data: data, encoding: .utf8)
        else {
            return nil
        }
        
        return str
    }
    
    func save(propertyName: String, value: String) -> String {
        var query = getQuery(propertyName: propertyName)
        query[kSecValueData as String] = value.data(using: .utf8)
        query[kSecAttrAccount as String] = propertyName
        SecItemAdd(query as CFDictionary, nil)
        return value
    }
        
    func getQuery(propertyName: String) -> [String: Any] {
        let query: [String: Any] = [
            kSecAttrService as String: service,
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: propertyName
        ]
        return query
    }
}
