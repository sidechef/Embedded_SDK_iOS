//
//  SCSDKCore.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/4/12.
//

import Foundation
import WebKit

let SC_LAST_REDIRECT_URL = "sc_last_redirect_url"

public class SCAuthenticator: NSObject {
    public static let shared: SCAuthenticator = SCAuthenticator()
    private(set) var partnerKey: String?
    private(set) var clientKey: String?
    private(set) var _apiKey: String?
    public var isInitialed: Bool {
        partnerKey != nil
    }

    var config: SCAuthConfigration?
    var appliances: [String]?
    
    override init() {
        super.init()
    }
    
    public func initSDK(apiKey: String, config: SCAuthConfigration) throws {
        try self.setApiKey(apiKey: apiKey)
        self.config = config
    }
    
    func setApiKey(apiKey: String) throws {
        guard let decodedData = Data(base64Encoded: apiKey) else {
            throw SCError.runtimeError(message: "API key format not correct.")
        }
        let dataArray = String(data: decodedData, encoding: .utf8)?.split(separator: ":")
        if dataArray!.count < 2 {
            throw SCError.runtimeError(message: "API key format not correct.")
        }
        partnerKey = String(dataArray![0])
        
        clientKey = String(dataArray![1])
        self._apiKey = apiKey
    }
}

// SCAuthenticator extension for web authentication
extension SCAuthenticator {
    func getSignature(key: String, timestamp: String, id: String, name: String?, email: String?, photoUrl: String?) -> String {
        // TODO: Add name and email support in server.
        let message = [id, photoUrl ?? "", timestamp].joined(separator: "")
        return message.hmac(algorithm: .SHA256, key: key)
    }
    
    public func getAuthUrl(redirect: Bool = true) -> String {
        var url = "\(SC_BASE_URL)/\(partnerKey ?? "")/account/signup/"
        if redirect {
            url.append(contentsOf: "?redirect=1")
        }
        return url
    }
    
    public func getAuthPayload() throws -> Data? {
        guard let clientKey = self.clientKey,
              let config = self.config else {
            throw SCError.runtimeError(message: "init SDK first")
        }
        
        let dateFormatter = ISO8601DateFormatter()
        let timestamp = dateFormatter.string(from: Date())
        
//        let signature = getSignature(key: clientKey, timestamp: timestamp, id: config.id, photoUrl: config.photoURL)
    
        let signature = getSignature(key: clientKey, timestamp: timestamp, id: config.id, name: config.name, email: config.email, photoUrl: config.photoURL)
        
        let jsonEncoder = JSONEncoder()
        let payload = SCAuthPayload(username: "lg$\(config.id)",
                              appliances: appliances,
                              timestamp: timestamp,
                              signature: signature)
        let jsonData = try jsonEncoder.encode(payload)
        let json = String(data: jsonData, encoding: String.Encoding.utf8)
        return (json?.data(using: .utf16))!
    }
    
    func authenticate(initialPage: SCInitialPage,
                      onComplete: @escaping (_ authInfo: SCAuthInfo) -> Void) {
        authenticate(initialPage: initialPage, onComplete: onComplete, onError: nil)
    }
    
    public func authenticateIfNeeded(initialPage: SCInitialPage,
                                     onComplete: @escaping (_ authInfo: SCAuthInfo) -> Void,
                                     onError: ( (_ error: Error) -> Void)?
    ) {
        let authUrl = getAuthUrl(redirect: false)
        if !SCCookieManager.shared.needReauthorize() {
            let url = UserDefaults.standard.value(forKey: SC_LAST_REDIRECT_URL) as? String
            onComplete(SCAuthInfo(url: getNextUrl(initialPage: initialPage, redirectUrl: url ?? authUrl)))
            return
        }
        print("Reauthorization needed, proceeding to authenticate.")
        authenticate(initialPage: initialPage, onComplete: onComplete, onError: onError)
    }
    
    func authenticateIfNeeded(onComplete: @escaping (_ authInfo: SCAuthInfo) -> Void,
                              onError: ( (_ error: Error) -> Void)?
    ) {
        authenticateIfNeeded(initialPage: .none, onComplete: onComplete, onError: onError)
    }
    
    func authenticate(initialPage: SCInitialPage,
                      onComplete: @escaping (_ authInfo: SCAuthInfo) -> Void,
                      onError: ( (_ error: Error) -> Void)?
    ) {
        let authUrl = getAuthUrl(redirect: false)
        guard let url = URL(string: authUrl) else {
            onError?(SCError.runtimeError(message: "failed when parse string \(authUrl) to String."))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody = try getAuthPayload()
        } catch {
            onError?(error)
            return
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                onError?(SCError.runtimeError(message: "response data is nil"))
                return
            }
            do {
                guard let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                      let redirectUrl = json["redirect"] as? String,
                      let userID = json["user_id"] as? Int
                else {
                    onError?(SCError.runtimeError(message: "failed when parse auth json."))
                    return
                }
                SCAccount.shared.userID = userID
                UserDefaults.standard.set(redirectUrl, forKey: SC_LAST_REDIRECT_URL)
                print("Login success")
                
                onComplete(SCAuthInfo(url: self.getNextUrl(initialPage: initialPage,
                                                      redirectUrl: redirectUrl)))
            } catch {
                onError?(error)
            }
        }.resume()
    }
    
    func getNextUrl(initialPage: SCInitialPage, redirectUrl: String?) -> String {
        let partnerPattern = (partnerKey != nil) ? "/\(partnerKey!)" : ""
        let url: String
        switch initialPage {
        case .recipe(id: let id):
            url = "\(SC_BASE_URL)\(partnerPattern)/recipes/\(id)/"
        case .mealPlan:
            url = "\(SC_BASE_URL)\(partnerPattern)/meal-planner/"
        case .grocery:
            url = "\(SC_BASE_URL)\(partnerPattern)/grocery/"
        case .none:
            url = redirectUrl ?? SC_BASE_URL
        }
        return url
    }
}
