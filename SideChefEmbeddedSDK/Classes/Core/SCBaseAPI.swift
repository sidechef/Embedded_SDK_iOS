//
//  SCAPI.swift
//  SCEmbeddedSDK
//
//  Created by Cosmo Young on 2022/8/25.
//

import UIKit

open class SCBaseAPI: NSObject {
    public func get(urlPattern: String, onComplete: @escaping (_ data: Data) -> Void,
                    onError: ( (_ error: Error) -> Void)?) {
        SCAuthenticator.shared.authenticateIfNeeded { authInfo in
            guard let url = URL(string: "\(SC_BASE_URL)\(urlPattern)") else {
                onError?(SCError(message: "url not correct."))
                return
            }
            var request = URLRequest(url: url)
            do {
                try SCCookieManager.shared.applyAuthCookies(request: &request)
            } catch {
                onError?(error)
                return
            }
            request.httpMethod = "GET"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(SCAccount.getUserAgent(), forHTTPHeaderField: "User-Agent")

            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    onError?(error)
                }
                guard let data = data else {
                    onError?(SCError.runtimeError(message: "response data is nil when request \(urlPattern)."))
                    return
                }
                onComplete(data)
            }.resume()
        } onError: { error in
            onError?(error)
        }
    }
}
