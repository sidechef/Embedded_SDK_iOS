//
//  SCCookieManager.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/8/5.
//

import WebKit

let AUTH_COOKIE_KEYS = ["csrftoken", "sessionid"]

public class SCCookieManager: NSObject {
    public static let shared: SCCookieManager = SCCookieManager()

    func getCookies() -> [HTTPCookie]? {
        let url = URL(string: SC_BASE_URL)!
        return HTTPCookieStorage.shared.cookies(for: url)?.filter({ cookie in
            AUTH_COOKIE_KEYS.contains(cookie.name)
        })
    }
    
    func needReauthorize() -> Bool {
        guard let cookies = getCookies() else {
//            print("No cookies available, reauthorization needed")
            return true
        }
        if cookies.isEmpty {
//            print("Cookie storage is empty, reauthorization needed")
            return true
        }
        for cookie in cookies {
            if let expiresDate = cookie.expiresDate, expiresDate.timeIntervalSince(Date()) < 60 * 5 {
//               print("Cookie is expiring soon, reauthorization needed")
               return true
            }
        }
//        print("Cookies are valid, no reauthorization needed", cookies)
        return false
    }

    func clearCookies() {
        let url = URL(string: SC_BASE_URL)!
        guard let cookieStorage = HTTPCookieStorage.shared.cookies(for: url) else {
            print("No cookies to clear")
            return
        }

        for cookie in cookieStorage {
            if AUTH_COOKIE_KEYS.contains(cookie.name) {
                HTTPCookieStorage.shared.deleteCookie(cookie)
                print("Deleted cookie:", cookie.name)
            }
        }
        print("Cleared authentication cookies.")
    }

    public func applyAuthCookies(webView: WKWebView) {
        if let cookies = getCookies() {
            let cookieStore = webView.configuration.websiteDataStore.httpCookieStore
            for cookie in cookies {
                cookieStore.setCookie(cookie)
            }
        }
    }
    
    public func applyAuthCookies(request: inout URLRequest) throws {
        guard let cookies = getCookies(), !cookies.isEmpty else {
            throw SCError.runtimeError(message: "No authentication cookies available.")
        }

        let cookieHeader = cookies.map { "\($0.name)=\($0.value)" }.joined(separator: "; ")
        request.setValue(cookieHeader, forHTTPHeaderField: "Cookie")
    }
}
