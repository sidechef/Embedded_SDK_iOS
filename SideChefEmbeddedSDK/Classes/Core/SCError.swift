//
//  Errors.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/8/8.
//

public struct SCError: Error {
    public var message: String?
    
    public static func runtimeError(message: String) -> SCError {
        SCError(message: message)
    }
}
