//
//  SCListModel.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/9/19.
//

import Foundation

public class SCListAPI<T>: SCBaseAPI where T: SCListResultProtocol {
    var next: String?
    var pageSize: Int
    var urlPattern: String
    
    public init(pageSize: Int, urlPattern: String) {
        self.pageSize = pageSize
        self.urlPattern = urlPattern
        super.init()
    }

    public func getNextPage(onComplete: @escaping (Result<T, Error>) -> Void) {
        let nextUrl = next ?? "\(urlPattern)?page=1&page_size=\(pageSize)"
        self.get(urlPattern: nextUrl) { data in
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                self.next = result.next
                onComplete(.success(result))
            } catch {
                onComplete(.failure(error))
            }

        } onError: { error in
            print("API call failed with error: \(error)")
            onComplete(.failure(error))
        }
    }
    
    public func reset() {
        next = nil
    }
}
