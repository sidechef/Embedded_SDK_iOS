//
//  SCEntities+API.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/9/19.
//

import Foundation

public protocol SCListResultProtocol: Decodable {
    associatedtype Element

    var count: Int { get }
    var next: String? { get }
    var previous: String? { get }
    var results: [Element] { get }
}
