//
//  Entities.swift
//  SCSDKCore
//
//  Created by Cosmo Young on 2022/4/13.
//

import Foundation

public enum SCInitialPage {
    case recipe(id: Int)
    case mealPlan
    case grocery
    case none
}

public struct SCAuthConfigration: Codable {
    var id: String
    var name: String?
    var email: String?
    var photoURL: String?
    
    public init(id: String, name: String?, email: String?, photoURL: String?) {
        self.id = id
        self.name = name
        self.email = email
        self.photoURL = photoURL
    }
}

public struct SCAuthPayload: Codable {
    var username: String
    var appliances: [String]?
    var timestamp: String
    var signature: String
}

public struct SCAuthInfo {
    public var url: String
}
