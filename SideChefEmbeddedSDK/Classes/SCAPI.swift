//
//  SCAPI.swift
//  SideChefEmbeddedSDK
//
//  Created by Cosmo Young on 2022/8/25.
//

import Foundation

/// A class that manages the SideChef API.
public class SCAPI: SCBaseAPI {
    /// A shared instance of the `SCAPI` class.
    public static let shared: SCAPI = SCAPI()
    
    /// A property that holds a list of popular recipes.
    ///
    /// The list is fetched using `SCListAPI` with a page size of 12 and a URL pattern for the API endpoint.
    public var homePopular = SCListAPI<SCListResult<SCRecipe>>(pageSize: 12, urlPattern: "/lg/home/popular/")
}
