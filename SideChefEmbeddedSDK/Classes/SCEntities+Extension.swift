//
//  SCEntities+Extension.swift
//  SideChefEmbeddedSDK
//
//  Created by Cosmo Young on 2023/2/7.
//

import Foundation

extension SCUserInfo {
    func toAuthConfig() -> SCAuthConfigration {
        SCAuthConfigration(id: id!, name: name, email: email, photoURL: photoURL)
    }
}
