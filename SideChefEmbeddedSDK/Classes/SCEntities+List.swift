//
//  SCEntities+List.swift
//  SideChefEmbeddedSDK
//
//  Created by Cosmo Young on 2023/2/7.
//

import Foundation

public struct SCListResult<T>: Codable, SCListResultProtocol where T: Codable {
    public var count: Int
    public var next: String?
    public var previous: String?
    public var results: [T]
}
