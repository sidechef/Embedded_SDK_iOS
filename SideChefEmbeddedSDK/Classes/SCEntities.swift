// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let sCRecipe = try SCRecipe(json)
//   let sCUserInfo = try SCUserInfo(json)

import Foundation

// MARK: - SCRecipe
public struct SCRecipe: Codable {
    public var id: Int?
    public var name, description: String?
    public var version: Int?
    public var coverPicURL: String?
    public var owner: SCRecipeOwner?
    public var countries, dishTypes, dishTags: [String]?
    public var dishTag: String?
    public var activeTime: Int?
    public var totalTime: String?
    public var servings: Int?
    public var currencyCurrency: String?
    public var currency: Int?
    public var reviewed: Bool?
    public var cookedCount: Int?
    public var recommended, saved: Bool?
    public var externalURL: String?
    public var ingredients: [SCRecipeIngredient]?
    public var steps: [SCStep]?
    public var liked: Bool?
    public var numLikes, numComments, numPhotos: Int?
    public var topComments: [SCTopComment]?
    public var topPhotos: [SCTopPhoto]?
    public var avgRating, rating: Double?
    public var numRating: Int?
    public var videoURL: String?
    public var coverPicURLSmall, coverPicURLMedium: String?
    public var numCookbooks: Int?
    public var suggested: [SCSuggested]?
    public var shareURL: String?
    public var buyURL: String?
    public var nutritionInformation: SCNutritionInformation?
    public var resources: [SCResource]?
    public var note: String?
    public var custom: Bool?
    public var recipeType, prepTime, cookTime: Int?
    public var communityPick, editorPick: Bool?
    public var quality: Int?
    public var trailerVideo: String?
    public var trailerVideoStreaming: String?
    public var playAudioTrackInVideo, premium, showGrocery: Bool?
    public var priceInCart: [SCPriceInCartElement]?
    public var defaultPriceInCart, pricePerServing: String?
    public var ingredientCount, requiredIngredientCount: Int?
    public var visibility: String?

    enum CodingKeys: String, CodingKey {
        case id, name, description, version
        case coverPicURL = "cover_pic_url"
        case owner, countries
        case dishTypes = "dish_types"
        case dishTags = "dish_tags"
        case dishTag = "dish_tag"
        case activeTime = "active_time"
        case totalTime = "total_time"
        case servings
        case currencyCurrency = "currency_currency"
        case currency, reviewed
        case cookedCount = "cooked_count"
        case recommended, saved
        case externalURL = "external_url"
        case ingredients, steps, liked
        case numLikes = "num_likes"
        case numComments = "num_comments"
        case numPhotos = "num_photos"
        case topComments = "top_comments"
        case topPhotos = "top_photos"
        case avgRating = "avg_rating"
        case rating
        case numRating = "num_rating"
        case videoURL = "video_url"
        case coverPicURLSmall = "cover_pic_url_small"
        case coverPicURLMedium = "cover_pic_url_medium"
        case numCookbooks = "num_cookbooks"
        case suggested
        case shareURL = "share_url"
        case buyURL = "buy_url"
        case nutritionInformation = "nutrition_information"
        case resources, note, custom
        case recipeType = "recipe_type"
        case prepTime = "prep_time"
        case cookTime = "cook_time"
        case communityPick = "community_pick"
        case editorPick = "editor_pick"
        case quality
        case trailerVideo = "trailer_video"
        case trailerVideoStreaming = "trailer_video_streaming"
        case playAudioTrackInVideo = "play_audio_track_in_video"
        case premium
        case showGrocery = "show_grocery"
        case priceInCart = "price_in_cart"
        case defaultPriceInCart = "default_price_in_cart"
        case pricePerServing = "price_per_serving"
        case ingredientCount = "ingredient_count"
        case requiredIngredientCount = "required_ingredient_count"
        case visibility
    }

    public init(id: Int?, name: String?, description: String?, version: Int?, coverPicURL: String?, owner: SCRecipeOwner?, countries: [String]?, dishTypes: [String]?, dishTags: [String]?, dishTag: String?, activeTime: Int?, totalTime: String?, servings: Int?, currencyCurrency: String?, currency: Int?, reviewed: Bool?, cookedCount: Int?, recommended: Bool?, saved: Bool?, externalURL: String?, ingredients: [SCRecipeIngredient]?, steps: [SCStep]?, liked: Bool?, numLikes: Int?, numComments: Int?, numPhotos: Int?, topComments: [SCTopComment]?, topPhotos: [SCTopPhoto]?, avgRating: Double?, rating: Double?, numRating: Int?, videoURL: String?, coverPicURLSmall: String?, coverPicURLMedium: String?, numCookbooks: Int?, suggested: [SCSuggested]?, shareURL: String?, buyURL: String?, nutritionInformation: SCNutritionInformation?, resources: [SCResource]?, note: String?, custom: Bool?, recipeType: Int?, prepTime: Int?, cookTime: Int?, communityPick: Bool?, editorPick: Bool?, quality: Int?, trailerVideo: String?, trailerVideoStreaming: String?, playAudioTrackInVideo: Bool?, premium: Bool?, showGrocery: Bool?, priceInCart: [SCPriceInCartElement]?, defaultPriceInCart: String?, pricePerServing: String?, ingredientCount: Int?, requiredIngredientCount: Int?, visibility: String?) {
        self.id = id
        self.name = name
        self.description = description
        self.version = version
        self.coverPicURL = coverPicURL
        self.owner = owner
        self.countries = countries
        self.dishTypes = dishTypes
        self.dishTags = dishTags
        self.dishTag = dishTag
        self.activeTime = activeTime
        self.totalTime = totalTime
        self.servings = servings
        self.currencyCurrency = currencyCurrency
        self.currency = currency
        self.reviewed = reviewed
        self.cookedCount = cookedCount
        self.recommended = recommended
        self.saved = saved
        self.externalURL = externalURL
        self.ingredients = ingredients
        self.steps = steps
        self.liked = liked
        self.numLikes = numLikes
        self.numComments = numComments
        self.numPhotos = numPhotos
        self.topComments = topComments
        self.topPhotos = topPhotos
        self.avgRating = avgRating
        self.rating = rating
        self.numRating = numRating
        self.videoURL = videoURL
        self.coverPicURLSmall = coverPicURLSmall
        self.coverPicURLMedium = coverPicURLMedium
        self.numCookbooks = numCookbooks
        self.suggested = suggested
        self.shareURL = shareURL
        self.buyURL = buyURL
        self.nutritionInformation = nutritionInformation
        self.resources = resources
        self.note = note
        self.custom = custom
        self.recipeType = recipeType
        self.prepTime = prepTime
        self.cookTime = cookTime
        self.communityPick = communityPick
        self.editorPick = editorPick
        self.quality = quality
        self.trailerVideo = trailerVideo
        self.trailerVideoStreaming = trailerVideoStreaming
        self.playAudioTrackInVideo = playAudioTrackInVideo
        self.premium = premium
        self.showGrocery = showGrocery
        self.priceInCart = priceInCart
        self.defaultPriceInCart = defaultPriceInCart
        self.pricePerServing = pricePerServing
        self.ingredientCount = ingredientCount
        self.requiredIngredientCount = requiredIngredientCount
        self.visibility = visibility
    }
}

// MARK: SCRecipe convenience initializers and mutators

public extension SCRecipe {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCRecipe.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        name: String?? = nil,
        description: String?? = nil,
        version: Int?? = nil,
        coverPicURL: String?? = nil,
        owner: SCRecipeOwner?? = nil,
        countries: [String]?? = nil,
        dishTypes: [String]?? = nil,
        dishTags: [String]?? = nil,
        dishTag: String?? = nil,
        activeTime: Int?? = nil,
        totalTime: String?? = nil,
        servings: Int?? = nil,
        currencyCurrency: String?? = nil,
        currency: Int?? = nil,
        reviewed: Bool?? = nil,
        cookedCount: Int?? = nil,
        recommended: Bool?? = nil,
        saved: Bool?? = nil,
        externalURL: String?? = nil,
        ingredients: [SCRecipeIngredient]?? = nil,
        steps: [SCStep]?? = nil,
        liked: Bool?? = nil,
        numLikes: Int?? = nil,
        numComments: Int?? = nil,
        numPhotos: Int?? = nil,
        topComments: [SCTopComment]?? = nil,
        topPhotos: [SCTopPhoto]?? = nil,
        avgRating: Double?? = nil,
        rating: Double?? = nil,
        numRating: Int?? = nil,
        videoURL: String?? = nil,
        coverPicURLSmall: String?? = nil,
        coverPicURLMedium: String?? = nil,
        numCookbooks: Int?? = nil,
        suggested: [SCSuggested]?? = nil,
        shareURL: String?? = nil,
        buyURL: String?? = nil,
        nutritionInformation: SCNutritionInformation?? = nil,
        resources: [SCResource]?? = nil,
        note: String?? = nil,
        custom: Bool?? = nil,
        recipeType: Int?? = nil,
        prepTime: Int?? = nil,
        cookTime: Int?? = nil,
        communityPick: Bool?? = nil,
        editorPick: Bool?? = nil,
        quality: Int?? = nil,
        trailerVideo: String?? = nil,
        trailerVideoStreaming: String?? = nil,
        playAudioTrackInVideo: Bool?? = nil,
        premium: Bool?? = nil,
        showGrocery: Bool?? = nil,
        priceInCart: [SCPriceInCartElement]?? = nil,
        defaultPriceInCart: String?? = nil,
        pricePerServing: String?? = nil,
        ingredientCount: Int?? = nil,
        requiredIngredientCount: Int?? = nil,
        visibility: String?? = nil
    ) -> SCRecipe {
        return SCRecipe(
            id: id ?? self.id,
            name: name ?? self.name,
            description: description ?? self.description,
            version: version ?? self.version,
            coverPicURL: coverPicURL ?? self.coverPicURL,
            owner: owner ?? self.owner,
            countries: countries ?? self.countries,
            dishTypes: dishTypes ?? self.dishTypes,
            dishTags: dishTags ?? self.dishTags,
            dishTag: dishTag ?? self.dishTag,
            activeTime: activeTime ?? self.activeTime,
            totalTime: totalTime ?? self.totalTime,
            servings: servings ?? self.servings,
            currencyCurrency: currencyCurrency ?? self.currencyCurrency,
            currency: currency ?? self.currency,
            reviewed: reviewed ?? self.reviewed,
            cookedCount: cookedCount ?? self.cookedCount,
            recommended: recommended ?? self.recommended,
            saved: saved ?? self.saved,
            externalURL: externalURL ?? self.externalURL,
            ingredients: ingredients ?? self.ingredients,
            steps: steps ?? self.steps,
            liked: liked ?? self.liked,
            numLikes: numLikes ?? self.numLikes,
            numComments: numComments ?? self.numComments,
            numPhotos: numPhotos ?? self.numPhotos,
            topComments: topComments ?? self.topComments,
            topPhotos: topPhotos ?? self.topPhotos,
            avgRating: avgRating ?? self.avgRating,
            rating: rating ?? self.rating,
            numRating: numRating ?? self.numRating,
            videoURL: videoURL ?? self.videoURL,
            coverPicURLSmall: coverPicURLSmall ?? self.coverPicURLSmall,
            coverPicURLMedium: coverPicURLMedium ?? self.coverPicURLMedium,
            numCookbooks: numCookbooks ?? self.numCookbooks,
            suggested: suggested ?? self.suggested,
            shareURL: shareURL ?? self.shareURL,
            buyURL: buyURL ?? self.buyURL,
            nutritionInformation: nutritionInformation ?? self.nutritionInformation,
            resources: resources ?? self.resources,
            note: note ?? self.note,
            custom: custom ?? self.custom,
            recipeType: recipeType ?? self.recipeType,
            prepTime: prepTime ?? self.prepTime,
            cookTime: cookTime ?? self.cookTime,
            communityPick: communityPick ?? self.communityPick,
            editorPick: editorPick ?? self.editorPick,
            quality: quality ?? self.quality,
            trailerVideo: trailerVideo ?? self.trailerVideo,
            trailerVideoStreaming: trailerVideoStreaming ?? self.trailerVideoStreaming,
            playAudioTrackInVideo: playAudioTrackInVideo ?? self.playAudioTrackInVideo,
            premium: premium ?? self.premium,
            showGrocery: showGrocery ?? self.showGrocery,
            priceInCart: priceInCart ?? self.priceInCart,
            defaultPriceInCart: defaultPriceInCart ?? self.defaultPriceInCart,
            pricePerServing: pricePerServing ?? self.pricePerServing,
            ingredientCount: ingredientCount ?? self.ingredientCount,
            requiredIngredientCount: requiredIngredientCount ?? self.requiredIngredientCount,
            visibility: visibility ?? self.visibility
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCRecipeIngredient
public struct SCRecipeIngredient: Codable {
    public var ingredientID, ingredientName, originalIngredientName, plural: String?
    public var parentID: String?
    public var ingredientOptional: Bool?
    public var category: String?
    public var showWeight: Bool?
    public var deal, unit: String?
    public var unitID: Int?
    public var unitSymbol, unitPlural, unitDisplayMode: String?
    public var preparations, qualifiers: [JSONAny]?
    public var products: [String]?
    public var imageURL, imageMediumURL, imageSmallURL: String?
    public var mightHave: Bool?

    enum CodingKeys: String, CodingKey {
        case ingredientID = "ingredient_id"
        case ingredientName = "ingredient_name"
        case originalIngredientName = "original_ingredient_name"
        case plural
        case parentID = "parent_id"
        case ingredientOptional = "optional"
        case category
        case showWeight = "show_weight"
        case deal, unit
        case unitID = "unit_id"
        case unitSymbol = "unit_symbol"
        case unitPlural = "unit_plural"
        case unitDisplayMode = "unit_display_mode"
        case preparations, qualifiers, products
        case imageURL = "image_url"
        case imageMediumURL = "image_medium_url"
        case imageSmallURL = "image_small_url"
        case mightHave = "might_have"
    }

    public init(ingredientID: String?, ingredientName: String?, originalIngredientName: String?, plural: String?, parentID: String?, ingredientOptional: Bool?, category: String?, showWeight: Bool?, deal: String?, unit: String?, unitID: Int?, unitSymbol: String?, unitPlural: String?, unitDisplayMode: String?, preparations: [JSONAny]?, qualifiers: [JSONAny]?, products: [String]?, imageURL: String?, imageMediumURL: String?, imageSmallURL: String?, mightHave: Bool?) {
        self.ingredientID = ingredientID
        self.ingredientName = ingredientName
        self.originalIngredientName = originalIngredientName
        self.plural = plural
        self.parentID = parentID
        self.ingredientOptional = ingredientOptional
        self.category = category
        self.showWeight = showWeight
        self.deal = deal
        self.unit = unit
        self.unitID = unitID
        self.unitSymbol = unitSymbol
        self.unitPlural = unitPlural
        self.unitDisplayMode = unitDisplayMode
        self.preparations = preparations
        self.qualifiers = qualifiers
        self.products = products
        self.imageURL = imageURL
        self.imageMediumURL = imageMediumURL
        self.imageSmallURL = imageSmallURL
        self.mightHave = mightHave
    }
}

// MARK: SCRecipeIngredient convenience initializers and mutators

public extension SCRecipeIngredient {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCRecipeIngredient.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        ingredientID: String?? = nil,
        ingredientName: String?? = nil,
        originalIngredientName: String?? = nil,
        plural: String?? = nil,
        parentID: String?? = nil,
        ingredientOptional: Bool?? = nil,
        category: String?? = nil,
        showWeight: Bool?? = nil,
        deal: String?? = nil,
        unit: String?? = nil,
        unitID: Int?? = nil,
        unitSymbol: String?? = nil,
        unitPlural: String?? = nil,
        unitDisplayMode: String?? = nil,
        preparations: [JSONAny]?? = nil,
        qualifiers: [JSONAny]?? = nil,
        products: [String]?? = nil,
        imageURL: String?? = nil,
        imageMediumURL: String?? = nil,
        imageSmallURL: String?? = nil,
        mightHave: Bool?? = nil
    ) -> SCRecipeIngredient {
        return SCRecipeIngredient(
            ingredientID: ingredientID ?? self.ingredientID,
            ingredientName: ingredientName ?? self.ingredientName,
            originalIngredientName: originalIngredientName ?? self.originalIngredientName,
            plural: plural ?? self.plural,
            parentID: parentID ?? self.parentID,
            ingredientOptional: ingredientOptional ?? self.ingredientOptional,
            category: category ?? self.category,
            showWeight: showWeight ?? self.showWeight,
            deal: deal ?? self.deal,
            unit: unit ?? self.unit,
            unitID: unitID ?? self.unitID,
            unitSymbol: unitSymbol ?? self.unitSymbol,
            unitPlural: unitPlural ?? self.unitPlural,
            unitDisplayMode: unitDisplayMode ?? self.unitDisplayMode,
            preparations: preparations ?? self.preparations,
            qualifiers: qualifiers ?? self.qualifiers,
            products: products ?? self.products,
            imageURL: imageURL ?? self.imageURL,
            imageMediumURL: imageMediumURL ?? self.imageMediumURL,
            imageSmallURL: imageSmallURL ?? self.imageSmallURL,
            mightHave: mightHave ?? self.mightHave
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCNutritionInformation
public struct SCNutritionInformation: Codable {
    public var calories, totalFat, saturatedFat, transFat: SCCalcium?
    public var cholesterol, sodium, totalCarbohydrate, protein: SCCalcium?
    public var dietaryFiber, sugars, calcium, iron: SCCalcium?
    public var potassium: SCCalcium?
    public var vitaminD: SCVitaminD?

    enum CodingKeys: String, CodingKey {
        case calories
        case totalFat = "total_fat"
        case saturatedFat = "saturated_fat"
        case transFat = "trans_fat"
        case cholesterol, sodium
        case totalCarbohydrate = "total_carbohydrate"
        case protein
        case dietaryFiber = "dietary_fiber"
        case sugars, calcium, iron, potassium
        case vitaminD = "vitamin_d"
    }

    public init(calories: SCCalcium?, totalFat: SCCalcium?, saturatedFat: SCCalcium?, transFat: SCCalcium?, cholesterol: SCCalcium?, sodium: SCCalcium?, totalCarbohydrate: SCCalcium?, protein: SCCalcium?, dietaryFiber: SCCalcium?, sugars: SCCalcium?, calcium: SCCalcium?, iron: SCCalcium?, potassium: SCCalcium?, vitaminD: SCVitaminD?) {
        self.calories = calories
        self.totalFat = totalFat
        self.saturatedFat = saturatedFat
        self.transFat = transFat
        self.cholesterol = cholesterol
        self.sodium = sodium
        self.totalCarbohydrate = totalCarbohydrate
        self.protein = protein
        self.dietaryFiber = dietaryFiber
        self.sugars = sugars
        self.calcium = calcium
        self.iron = iron
        self.potassium = potassium
        self.vitaminD = vitaminD
    }
}

// MARK: SCNutritionInformation convenience initializers and mutators

public extension SCNutritionInformation {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCNutritionInformation.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        calories: SCCalcium?? = nil,
        totalFat: SCCalcium?? = nil,
        saturatedFat: SCCalcium?? = nil,
        transFat: SCCalcium?? = nil,
        cholesterol: SCCalcium?? = nil,
        sodium: SCCalcium?? = nil,
        totalCarbohydrate: SCCalcium?? = nil,
        protein: SCCalcium?? = nil,
        dietaryFiber: SCCalcium?? = nil,
        sugars: SCCalcium?? = nil,
        calcium: SCCalcium?? = nil,
        iron: SCCalcium?? = nil,
        potassium: SCCalcium?? = nil,
        vitaminD: SCVitaminD?? = nil
    ) -> SCNutritionInformation {
        return SCNutritionInformation(
            calories: calories ?? self.calories,
            totalFat: totalFat ?? self.totalFat,
            saturatedFat: saturatedFat ?? self.saturatedFat,
            transFat: transFat ?? self.transFat,
            cholesterol: cholesterol ?? self.cholesterol,
            sodium: sodium ?? self.sodium,
            totalCarbohydrate: totalCarbohydrate ?? self.totalCarbohydrate,
            protein: protein ?? self.protein,
            dietaryFiber: dietaryFiber ?? self.dietaryFiber,
            sugars: sugars ?? self.sugars,
            calcium: calcium ?? self.calcium,
            iron: iron ?? self.iron,
            potassium: potassium ?? self.potassium,
            vitaminD: vitaminD ?? self.vitaminD
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCCalcium
public struct SCCalcium: Codable {
    public var unit: String?
    public var value: Double?
    public var dailyValuePercentage: Int?

    enum CodingKeys: String, CodingKey {
        case unit, value
        case dailyValuePercentage = "daily_value_percentage"
    }

    public init(unit: String?, value: Double?, dailyValuePercentage: Int?) {
        self.unit = unit
        self.value = value
        self.dailyValuePercentage = dailyValuePercentage
    }
}

// MARK: SCCalcium convenience initializers and mutators

public extension SCCalcium {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCCalcium.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        unit: String?? = nil,
        value: Double?? = nil,
        dailyValuePercentage: Int?? = nil
    ) -> SCCalcium {
        return SCCalcium(
            unit: unit ?? self.unit,
            value: value ?? self.value,
            dailyValuePercentage: dailyValuePercentage ?? self.dailyValuePercentage
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCVitaminD
public struct SCVitaminD: Codable {
    public var unit, value: String?
    public var dailyValuePercentage: Int?

    enum CodingKeys: String, CodingKey {
        case unit, value
        case dailyValuePercentage = "daily_value_percentage"
    }

    public init(unit: String?, value: String?, dailyValuePercentage: Int?) {
        self.unit = unit
        self.value = value
        self.dailyValuePercentage = dailyValuePercentage
    }
}

// MARK: SCVitaminD convenience initializers and mutators

public extension SCVitaminD {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCVitaminD.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        unit: String?? = nil,
        value: String?? = nil,
        dailyValuePercentage: Int?? = nil
    ) -> SCVitaminD {
        return SCVitaminD(
            unit: unit ?? self.unit,
            value: value ?? self.value,
            dailyValuePercentage: dailyValuePercentage ?? self.dailyValuePercentage
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCRecipeOwner
public struct SCRecipeOwner: Codable {
    public var id, kolID, fullName: String?
    public var photoURL, photoURLLarge, photoURLSmall: String?
    public var websiteURL: String?
    public var personalSign: String?

    enum CodingKeys: String, CodingKey {
        case id
        case kolID = "kol_id"
        case fullName = "full_name"
        case photoURL = "photo_url"
        case photoURLLarge = "photo_url_large"
        case photoURLSmall = "photo_url_small"
        case websiteURL = "website_url"
        case personalSign = "personal_sign"
    }

    public init(id: String?, kolID: String?, fullName: String?, photoURL: String?, photoURLLarge: String?, photoURLSmall: String?, websiteURL: String?, personalSign: String?) {
        self.id = id
        self.kolID = kolID
        self.fullName = fullName
        self.photoURL = photoURL
        self.photoURLLarge = photoURLLarge
        self.photoURLSmall = photoURLSmall
        self.websiteURL = websiteURL
        self.personalSign = personalSign
    }
}

// MARK: SCRecipeOwner convenience initializers and mutators

public extension SCRecipeOwner {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCRecipeOwner.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: String?? = nil,
        kolID: String?? = nil,
        fullName: String?? = nil,
        photoURL: String?? = nil,
        photoURLLarge: String?? = nil,
        photoURLSmall: String?? = nil,
        websiteURL: String?? = nil,
        personalSign: String?? = nil
    ) -> SCRecipeOwner {
        return SCRecipeOwner(
            id: id ?? self.id,
            kolID: kolID ?? self.kolID,
            fullName: fullName ?? self.fullName,
            photoURL: photoURL ?? self.photoURL,
            photoURLLarge: photoURLLarge ?? self.photoURLLarge,
            photoURLSmall: photoURLSmall ?? self.photoURLSmall,
            websiteURL: websiteURL ?? self.websiteURL,
            personalSign: personalSign ?? self.personalSign
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCPriceInCartElement
public struct SCPriceInCartElement: Codable {
    public var servings: Int?
    public var price: String?

    public init(servings: Int?, price: String?) {
        self.servings = servings
        self.price = price
    }
}

// MARK: SCPriceInCartElement convenience initializers and mutators

public extension SCPriceInCartElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCPriceInCartElement.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        servings: Int?? = nil,
        price: String?? = nil
    ) -> SCPriceInCartElement {
        return SCPriceInCartElement(
            servings: servings ?? self.servings,
            price: price ?? self.price
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCResource
public struct SCResource: Codable {
    public var name: String?
    public var image: String?

    public init(name: String?, image: String?) {
        self.name = name
        self.image = image
    }
}

// MARK: SCResource convenience initializers and mutators

public extension SCResource {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCResource.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        name: String?? = nil,
        image: String?? = nil
    ) -> SCResource {
        return SCResource(
            name: name ?? self.name,
            image: image ?? self.image
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCStep
public struct SCStep: Codable {
    public var id: Int?
    public var guid: String?
    public var duration: Int?
    public var description, fullDescription, descriptionRaw, stepTip: String?
    public var photoURL: String?
    public var tip: String?
    public var ingredients: [SCStepIngredient]?
    public var timerLabel: String?
    public var videos, variants: [JSONAny]?
    public var video: SCVideo?
    public var streamingVideo: String?

    enum CodingKeys: String, CodingKey {
        case id, guid, duration, description
        case fullDescription = "full_description"
        case descriptionRaw = "description_raw"
        case stepTip = "step_tip"
        case photoURL = "photo_url"
        case tip, ingredients
        case timerLabel = "timer_label"
        case videos, variants, video
        case streamingVideo = "streaming_video"
    }

    public init(id: Int?, guid: String?, duration: Int?, description: String?, fullDescription: String?, descriptionRaw: String?, stepTip: String?, photoURL: String?, tip: String?, ingredients: [SCStepIngredient]?, timerLabel: String?, videos: [JSONAny]?, variants: [JSONAny]?, video: SCVideo?, streamingVideo: String?) {
        self.id = id
        self.guid = guid
        self.duration = duration
        self.description = description
        self.fullDescription = fullDescription
        self.descriptionRaw = descriptionRaw
        self.stepTip = stepTip
        self.photoURL = photoURL
        self.tip = tip
        self.ingredients = ingredients
        self.timerLabel = timerLabel
        self.videos = videos
        self.variants = variants
        self.video = video
        self.streamingVideo = streamingVideo
    }
}

// MARK: SCStep convenience initializers and mutators

public extension SCStep {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCStep.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        guid: String?? = nil,
        duration: Int?? = nil,
        description: String?? = nil,
        fullDescription: String?? = nil,
        descriptionRaw: String?? = nil,
        stepTip: String?? = nil,
        photoURL: String?? = nil,
        tip: String?? = nil,
        ingredients: [SCStepIngredient]?? = nil,
        timerLabel: String?? = nil,
        videos: [JSONAny]?? = nil,
        variants: [JSONAny]?? = nil,
        video: SCVideo?? = nil,
        streamingVideo: String?? = nil
    ) -> SCStep {
        return SCStep(
            id: id ?? self.id,
            guid: guid ?? self.guid,
            duration: duration ?? self.duration,
            description: description ?? self.description,
            fullDescription: fullDescription ?? self.fullDescription,
            descriptionRaw: descriptionRaw ?? self.descriptionRaw,
            stepTip: stepTip ?? self.stepTip,
            photoURL: photoURL ?? self.photoURL,
            tip: tip ?? self.tip,
            ingredients: ingredients ?? self.ingredients,
            timerLabel: timerLabel ?? self.timerLabel,
            videos: videos ?? self.videos,
            variants: variants ?? self.variants,
            video: video ?? self.video,
            streamingVideo: streamingVideo ?? self.streamingVideo
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCStepIngredient
public struct SCStepIngredient: Codable {
    public var ingredientID, ingredientName, plural, deal: String?
    public var unit: String?
    public var unitID: Int?
    public var unitSymbol, unitPlural: String?
    public var preparations: [JSONAny]?
    public var id: Int?

    enum CodingKeys: String, CodingKey {
        case ingredientID = "ingredient_id"
        case ingredientName = "ingredient_name"
        case plural, deal, unit
        case unitID = "unit_id"
        case unitSymbol = "unit_symbol"
        case unitPlural = "unit_plural"
        case preparations, id
    }

    public init(ingredientID: String?, ingredientName: String?, plural: String?, deal: String?, unit: String?, unitID: Int?, unitSymbol: String?, unitPlural: String?, preparations: [JSONAny]?, id: Int?) {
        self.ingredientID = ingredientID
        self.ingredientName = ingredientName
        self.plural = plural
        self.deal = deal
        self.unit = unit
        self.unitID = unitID
        self.unitSymbol = unitSymbol
        self.unitPlural = unitPlural
        self.preparations = preparations
        self.id = id
    }
}

// MARK: SCStepIngredient convenience initializers and mutators

public extension SCStepIngredient {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCStepIngredient.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        ingredientID: String?? = nil,
        ingredientName: String?? = nil,
        plural: String?? = nil,
        deal: String?? = nil,
        unit: String?? = nil,
        unitID: Int?? = nil,
        unitSymbol: String?? = nil,
        unitPlural: String?? = nil,
        preparations: [JSONAny]?? = nil,
        id: Int?? = nil
    ) -> SCStepIngredient {
        return SCStepIngredient(
            ingredientID: ingredientID ?? self.ingredientID,
            ingredientName: ingredientName ?? self.ingredientName,
            plural: plural ?? self.plural,
            deal: deal ?? self.deal,
            unit: unit ?? self.unit,
            unitID: unitID ?? self.unitID,
            unitSymbol: unitSymbol ?? self.unitSymbol,
            unitPlural: unitPlural ?? self.unitPlural,
            preparations: preparations ?? self.preparations,
            id: id ?? self.id
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCVideo
public struct SCVideo: Codable {
    public var url: String?

    public init(url: String?) {
        self.url = url
    }
}

// MARK: SCVideo convenience initializers and mutators

public extension SCVideo {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCVideo.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        url: String?? = nil
    ) -> SCVideo {
        return SCVideo(
            url: url ?? self.url
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCSuggested
public struct SCSuggested: Codable {
    public var id: Int?
    public var name: String?
    public var owner: SCSuggestedOwner?
    public var pricePerServing: String?
    public var showGrocery: Bool?
    public var priceInCart: SCSuggestedPriceInCart?
    public var activeTime: Double?
    public var cookTime, prepTime: JSONNull?
    public var coverPicURL, coverPicURLOrigin, coverPicURLSmall: String?
    public var totalTime, displayTime, ownerSlug: String?
    public var dishTags: [String]?
    public var dishTag: String?
    public var premium: Bool?
    public var servings: Int?
    public var avgRating: Double?
    public var defaultPriceInCart: String?
    public var ingredientCount: Int?
    public var ingredientCountDisplay: String?
    public var requiredIngredientCount: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, owner
        case pricePerServing = "price_per_serving"
        case showGrocery = "show_grocery"
        case priceInCart = "price_in_cart"
        case activeTime = "active_time"
        case cookTime = "cook_time"
        case prepTime = "prep_time"
        case coverPicURL = "cover_pic_url"
        case coverPicURLOrigin = "cover_pic_url_origin"
        case coverPicURLSmall = "cover_pic_url_small"
        case totalTime = "total_time"
        case displayTime = "display_time"
        case ownerSlug = "owner_slug"
        case dishTags = "dish_tags"
        case dishTag = "dish_tag"
        case premium, servings
        case avgRating = "avg_rating"
        case defaultPriceInCart = "default_price_in_cart"
        case ingredientCount = "ingredient_count"
        case ingredientCountDisplay = "ingredient_count_display"
        case requiredIngredientCount = "required_ingredient_count"
    }

    public init(id: Int?, name: String?, owner: SCSuggestedOwner?, pricePerServing: String?, showGrocery: Bool?, priceInCart: SCSuggestedPriceInCart?, activeTime: Double?, cookTime: JSONNull?, prepTime: JSONNull?, coverPicURL: String?, coverPicURLOrigin: String?, coverPicURLSmall: String?, totalTime: String?, displayTime: String?, ownerSlug: String?, dishTags: [String]?, dishTag: String?, premium: Bool?, servings: Int?, avgRating: Double?, defaultPriceInCart: String?, ingredientCount: Int?, ingredientCountDisplay: String?, requiredIngredientCount: Int?) {
        self.id = id
        self.name = name
        self.owner = owner
        self.pricePerServing = pricePerServing
        self.showGrocery = showGrocery
        self.priceInCart = priceInCart
        self.activeTime = activeTime
        self.cookTime = cookTime
        self.prepTime = prepTime
        self.coverPicURL = coverPicURL
        self.coverPicURLOrigin = coverPicURLOrigin
        self.coverPicURLSmall = coverPicURLSmall
        self.totalTime = totalTime
        self.displayTime = displayTime
        self.ownerSlug = ownerSlug
        self.dishTags = dishTags
        self.dishTag = dishTag
        self.premium = premium
        self.servings = servings
        self.avgRating = avgRating
        self.defaultPriceInCart = defaultPriceInCart
        self.ingredientCount = ingredientCount
        self.ingredientCountDisplay = ingredientCountDisplay
        self.requiredIngredientCount = requiredIngredientCount
    }
}

// MARK: SCSuggested convenience initializers and mutators

public extension SCSuggested {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCSuggested.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        name: String?? = nil,
        owner: SCSuggestedOwner?? = nil,
        pricePerServing: String?? = nil,
        showGrocery: Bool?? = nil,
        priceInCart: SCSuggestedPriceInCart?? = nil,
        activeTime: Double?? = nil,
        cookTime: JSONNull?? = nil,
        prepTime: JSONNull?? = nil,
        coverPicURL: String?? = nil,
        coverPicURLOrigin: String?? = nil,
        coverPicURLSmall: String?? = nil,
        totalTime: String?? = nil,
        displayTime: String?? = nil,
        ownerSlug: String?? = nil,
        dishTags: [String]?? = nil,
        dishTag: String?? = nil,
        premium: Bool?? = nil,
        servings: Int?? = nil,
        avgRating: Double?? = nil,
        defaultPriceInCart: String?? = nil,
        ingredientCount: Int?? = nil,
        ingredientCountDisplay: String?? = nil,
        requiredIngredientCount: Int?? = nil
    ) -> SCSuggested {
        return SCSuggested(
            id: id ?? self.id,
            name: name ?? self.name,
            owner: owner ?? self.owner,
            pricePerServing: pricePerServing ?? self.pricePerServing,
            showGrocery: showGrocery ?? self.showGrocery,
            priceInCart: priceInCart ?? self.priceInCart,
            activeTime: activeTime ?? self.activeTime,
            cookTime: cookTime ?? self.cookTime,
            prepTime: prepTime ?? self.prepTime,
            coverPicURL: coverPicURL ?? self.coverPicURL,
            coverPicURLOrigin: coverPicURLOrigin ?? self.coverPicURLOrigin,
            coverPicURLSmall: coverPicURLSmall ?? self.coverPicURLSmall,
            totalTime: totalTime ?? self.totalTime,
            displayTime: displayTime ?? self.displayTime,
            ownerSlug: ownerSlug ?? self.ownerSlug,
            dishTags: dishTags ?? self.dishTags,
            dishTag: dishTag ?? self.dishTag,
            premium: premium ?? self.premium,
            servings: servings ?? self.servings,
            avgRating: avgRating ?? self.avgRating,
            defaultPriceInCart: defaultPriceInCart ?? self.defaultPriceInCart,
            ingredientCount: ingredientCount ?? self.ingredientCount,
            ingredientCountDisplay: ingredientCountDisplay ?? self.ingredientCountDisplay,
            requiredIngredientCount: requiredIngredientCount ?? self.requiredIngredientCount
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCSuggestedOwner
public struct SCSuggestedOwner: Codable {
    public var id, kolID, firstName, fullName: String?
    public var photoURL, photoURLSmall: String?

    enum CodingKeys: String, CodingKey {
        case id
        case kolID = "kol_id"
        case firstName = "first_name"
        case fullName = "full_name"
        case photoURL = "photo_url"
        case photoURLSmall = "photo_url_small"
    }

    public init(id: String?, kolID: String?, firstName: String?, fullName: String?, photoURL: String?, photoURLSmall: String?) {
        self.id = id
        self.kolID = kolID
        self.firstName = firstName
        self.fullName = fullName
        self.photoURL = photoURL
        self.photoURLSmall = photoURLSmall
    }
}

// MARK: SCSuggestedOwner convenience initializers and mutators

public extension SCSuggestedOwner {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCSuggestedOwner.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: String?? = nil,
        kolID: String?? = nil,
        firstName: String?? = nil,
        fullName: String?? = nil,
        photoURL: String?? = nil,
        photoURLSmall: String?? = nil
    ) -> SCSuggestedOwner {
        return SCSuggestedOwner(
            id: id ?? self.id,
            kolID: kolID ?? self.kolID,
            firstName: firstName ?? self.firstName,
            fullName: fullName ?? self.fullName,
            photoURL: photoURL ?? self.photoURL,
            photoURLSmall: photoURLSmall ?? self.photoURLSmall
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCSuggestedPriceInCart
public struct SCSuggestedPriceInCart: Codable {
    public var serving1, serving2, serving3, serving4: Double?
    public var serving5, serving6, serving8, serving10: Double?
    public var serving12, serving14, serving16: Double?

    public init(serving1: Double?, serving2: Double?, serving3: Double?, serving4: Double?, serving5: Double?, serving6: Double?, serving8: Double?, serving10: Double?, serving12: Double?, serving14: Double?, serving16: Double?) {
        self.serving1 = serving1
        self.serving2 = serving2
        self.serving3 = serving3
        self.serving4 = serving4
        self.serving5 = serving5
        self.serving6 = serving6
        self.serving8 = serving8
        self.serving10 = serving10
        self.serving12 = serving12
        self.serving14 = serving14
        self.serving16 = serving16
    }
}

// MARK: SCSuggestedPriceInCart convenience initializers and mutators

public extension SCSuggestedPriceInCart {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCSuggestedPriceInCart.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        serving1: Double?? = nil,
        serving2: Double?? = nil,
        serving3: Double?? = nil,
        serving4: Double?? = nil,
        serving5: Double?? = nil,
        serving6: Double?? = nil,
        serving8: Double?? = nil,
        serving10: Double?? = nil,
        serving12: Double?? = nil,
        serving14: Double?? = nil,
        serving16: Double?? = nil
    ) -> SCSuggestedPriceInCart {
        return SCSuggestedPriceInCart(
            serving1: serving1 ?? self.serving1,
            serving2: serving2 ?? self.serving2,
            serving3: serving3 ?? self.serving3,
            serving4: serving4 ?? self.serving4,
            serving5: serving5 ?? self.serving5,
            serving6: serving6 ?? self.serving6,
            serving8: serving8 ?? self.serving8,
            serving10: serving10 ?? self.serving10,
            serving12: serving12 ?? self.serving12,
            serving14: serving14 ?? self.serving14,
            serving16: serving16 ?? self.serving16
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCTopComment
public struct SCTopComment: Codable {
    public var id: Int?
    public var text: String?
    public var star: Double?
    public var photoURL: String?
    public var user: SCUser?
    public var isFlagged: Bool?
    public var createdAt: Date?

    enum CodingKeys: String, CodingKey {
        case id, text, star
        case photoURL = "photo_url"
        case user
        case isFlagged = "is_flagged"
        case createdAt = "created_at"
    }

    public init(id: Int?, text: String?, star: Double?, photoURL: String?, user: SCUser?, isFlagged: Bool?, createdAt: Date?) {
        self.id = id
        self.text = text
        self.star = star
        self.photoURL = photoURL
        self.user = user
        self.isFlagged = isFlagged
        self.createdAt = createdAt
    }
}

// MARK: SCTopComment convenience initializers and mutators

public extension SCTopComment {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCTopComment.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        text: String?? = nil,
        star: Double?? = nil,
        photoURL: String?? = nil,
        user: SCUser?? = nil,
        isFlagged: Bool?? = nil,
        createdAt: Date?? = nil
    ) -> SCTopComment {
        return SCTopComment(
            id: id ?? self.id,
            text: text ?? self.text,
            star: star ?? self.star,
            photoURL: photoURL ?? self.photoURL,
            user: user ?? self.user,
            isFlagged: isFlagged ?? self.isFlagged,
            createdAt: createdAt ?? self.createdAt
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCUser
public struct SCUser: Codable {
    public var id: Int?
    public var firstName, fullName: String?
    public var photoURL, photoURLSmall: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case fullName = "full_name"
        case photoURL = "photo_url"
        case photoURLSmall = "photo_url_small"
    }

    public init(id: Int?, firstName: String?, fullName: String?, photoURL: String?, photoURLSmall: String?) {
        self.id = id
        self.firstName = firstName
        self.fullName = fullName
        self.photoURL = photoURL
        self.photoURLSmall = photoURLSmall
    }
}

// MARK: SCUser convenience initializers and mutators

public extension SCUser {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCUser.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        firstName: String?? = nil,
        fullName: String?? = nil,
        photoURL: String?? = nil,
        photoURLSmall: String?? = nil
    ) -> SCUser {
        return SCUser(
            id: id ?? self.id,
            firstName: firstName ?? self.firstName,
            fullName: fullName ?? self.fullName,
            photoURL: photoURL ?? self.photoURL,
            photoURLSmall: photoURLSmall ?? self.photoURLSmall
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCTopPhoto
public struct SCTopPhoto: Codable {
    public var id: Int?
    public var type: String?
    public var photoURL: String?
    public var createdAt: Date?
    public var user: SCUser?
    public var isFlagged, isLiked, isOwner: Bool?
    public var likeCount: Int?

    enum CodingKeys: String, CodingKey {
        case id, type
        case photoURL = "photo_url"
        case createdAt = "created_at"
        case user
        case isFlagged = "is_flagged"
        case isLiked = "is_liked"
        case isOwner = "is_owner"
        case likeCount = "like_count"
    }

    public init(id: Int?, type: String?, photoURL: String?, createdAt: Date?, user: SCUser?, isFlagged: Bool?, isLiked: Bool?, isOwner: Bool?, likeCount: Int?) {
        self.id = id
        self.type = type
        self.photoURL = photoURL
        self.createdAt = createdAt
        self.user = user
        self.isFlagged = isFlagged
        self.isLiked = isLiked
        self.isOwner = isOwner
        self.likeCount = likeCount
    }
}

// MARK: SCTopPhoto convenience initializers and mutators

public extension SCTopPhoto {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCTopPhoto.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: Int?? = nil,
        type: String?? = nil,
        photoURL: String?? = nil,
        createdAt: Date?? = nil,
        user: SCUser?? = nil,
        isFlagged: Bool?? = nil,
        isLiked: Bool?? = nil,
        isOwner: Bool?? = nil,
        likeCount: Int?? = nil
    ) -> SCTopPhoto {
        return SCTopPhoto(
            id: id ?? self.id,
            type: type ?? self.type,
            photoURL: photoURL ?? self.photoURL,
            createdAt: createdAt ?? self.createdAt,
            user: user ?? self.user,
            isFlagged: isFlagged ?? self.isFlagged,
            isLiked: isLiked ?? self.isLiked,
            isOwner: isOwner ?? self.isOwner,
            likeCount: likeCount ?? self.likeCount
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - SCUserInfo
public struct SCUserInfo: Codable {
    public var id, name: String?
    public var photoURL: String?
    public var email: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case photoURL = "photo_url"
        case email
    }

    public init(id: String?, name: String?, photoURL: String?, email: String?) {
        self.id = id
        self.name = name
        self.photoURL = photoURL
        self.email = email
    }
}

// MARK: SCUserInfo convenience initializers and mutators

public extension SCUserInfo {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SCUserInfo.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        id: String?? = nil,
        name: String?? = nil,
        photoURL: String?? = nil,
        email: String?? = nil
    ) -> SCUserInfo {
        return SCUserInfo(
            id: id ?? self.id,
            name: name ?? self.name,
            photoURL: photoURL ?? self.photoURL,
            email: email ?? self.email
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Encode/decode helpers

public class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

public class JSONAny: Codable {

    public let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
