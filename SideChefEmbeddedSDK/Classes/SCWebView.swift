//
//  SCWebView.swift
//  SideChefEmbeddedSDK
//
//  Created by Cosmo Young on 2022/4/2.
//

import UIKit
import WebKit
import Foundation

/// A class that represents an embedded SideChef web view.
@objc
public class SCWebView: UIView, WKNavigationDelegate {
    var webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
    public var progressView: UIProgressView = UIProgressView()
    var activityIndicator = UIActivityIndicatorView()
    private var initialPage: SCInitialPage = .none
    private var observation: NSKeyValueObservation?

    private var mHasProgress = false
    /// A property that indicates whether the progress view is visible.
    public var hasProgress: Bool {
        get {
            return mHasProgress
        }
        set (newVal) {
            mHasProgress = newVal
            self.progressView.isHidden = !mHasProgress
        }
    }

    /// Initializes a new instance of `SCWebView` with the specified initial page.
    ///
    /// - Parameter initialPage: The initial page to load in the web view.
    public init?(initialPage: SCInitialPage = .none) {
        if !SCAuthenticator.shared.isInitialed {
            return nil
        }
        super.init(frame: CGRect.zero)
        self.initialPage = initialPage
        self.initial()
    }
    required public init?(coder: NSCoder) {
        if !SCAuthenticator.shared.isInitialed {
            return nil
        }
        super.init(coder: coder)
        self.initial()
    }

    /// Sets up the web view and its subviews.
    private func initial() {
        self.backgroundColor = .white

        let config = WKWebViewConfiguration()

        webView = WKWebView(frame: CGRect.zero, configuration: config)
        addSubview(webView)

        webView.navigationDelegate = self

        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()

        self.addSubview(progressView)
        self.progressView.isHidden = !hasProgress
        progressView.bindWidthToSuperviewBounds()

        loadUrl()

        observation = webView.observe(\WKWebView.estimatedProgress, options: .new) { _, change in
            guard let value = change.newValue else {
                return
            }
            DispatchQueue.main.async() {
                self.progressView.setProgress(Float(value), animated: true)
            }
        }
    }

    deinit {
        self.observation = nil
    }

    /// Loads the URL in the web view.
    private func loadUrl() {
        SCAuthenticator.shared.authenticateIfNeeded(initialPage: self.initialPage) { authInfo in
            let request = URLRequest(url: URL(string: authInfo.url)!)
            DispatchQueue.main.async() {
                self.webView.customUserAgent = SCAccount.getUserAgent()
                SCCookieManager.shared.applyAuthCookies(webView: self.webView)
                self.webView.load(request)
            }
        } onError: { error in
            print("SCSDK auth for webview failed: ", error)
        }

    }

    /// Lays out the subviews of the web view.
    public override func layoutSubviews() {
        super.layoutSubviews()

        webView.frame = self.bounds
        activityIndicator.center = self.center
    }

    /// Called when the web view finishes loading a page.
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
        self.progressView.setProgress(1, animated: true)
    }

    /// Called when the web view starts loading a page.
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.progressView.progress = 0
    }
}

public extension SCWebView {
    /// Attempts to navigate back in the web view, if possible.
    /// - Returns: Boolean indicating whether the navigation was handled internally.
    @discardableResult
    func goBackIfNeeded() -> Bool {
        if webView.canGoBack {
            webView.goBack()
            return true
        }
        return false
    }
}
