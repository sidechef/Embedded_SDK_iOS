//
//  SideChefEmbeddedSDK.swift
//  SideChefEmbeddedSDK
//
//  Created by Cosmo Young on 2022/8/25.
//
/// A class that manages the SideChef Embedded SDK configuration.
public class SideChefEmbeddedSDK {
    /// Configures the SideChef Embedded SDK with the provided API key and user information.
    ///
    /// - Parameters:
    ///   - apiKey: The API key to use for authentication.
    ///   - userInfo: The user information to use for configuration.
    public static func configure(apiKey: String, userInfo: SCUserInfo) {
        do {
            try SCAuthenticator.shared.initSDK(apiKey: apiKey, config: userInfo.toAuthConfig())
            print("SideChef SDK initialized successfully.")
        } catch {
            print("SideChef SDK initial failed with error: ", error)
        }
    }
}
