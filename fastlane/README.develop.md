
# Create a private library with iOS pods Via Gitlab
1. Establishing a local code repository and adding a remote spec repo can be understood as establishing the association with a remote spec repo.
    - `$cd sidechef-repo/`
    - `$pod repo add SideChefEmbeddedSDK https://gitlab.com/sidechef/sidechef-repo.git`
2. Check the local repo
    - `$pod repo list`

    You should see SideChefEmbeddedSDK in the list.

3. Create a private CocoaPods library.
    - `$cd ~/SideChefEmbeddedSDK`
    - `$pod lib create SideChefEmbeddedSDK`

4. Create a git repo in gitlab and push the code.
    - `$cd ~/SideChefEmbeddedSDK`
    - `$git init`

    - `$git remote add origin "https://gitlab.com/sidechef/Embedded_SDK_iOS.git"`
    - `$git commit -m "init project"`
    - `$git push -u origin master/main/etc`

5. Package and push
    - `$pod spec lint SideChefEmbeddedSDK.podspec --allow-warnings`
    - `$git tag -m "first init project" 0.1.0`
    - `$git push --tags"`

6. Verify the validity of the podspec file on local
    - `$pod lib lint SideChefEmbeddedSDK.podspec --allow-warnings`


7. Submit
    - `$pod repo push SideChefEmbeddedSDK SideChefEmbeddedSDK.podspec --allow-warnings`

8. Verify
    - `$pod search`



# Deploy SideChefEmbeddedSDK
For deploy we using fastlane action to do auto release it.


Just need to run `bundle exec fastlane ios deploy` to release next version.

For more information: [Fastlane](https://docs.fastlane.tools/)
