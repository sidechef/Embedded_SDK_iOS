quicktype -o ../SideChefEmbeddedSDK/Classes/SCEntities.swift \
        --no-enums \
        --access-level public \
        --type-prefix SC \
        --mutable-properties \
        --coding-keys \
        --all-properties-optional \
        --no-alamofire \
        --src-lang schema \
        ./json \
